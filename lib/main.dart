import 'dart:io';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' as http;
import 'package:async/async.dart';
import 'package:path/path.dart';
import 'package:dio/dio.dart' as http_dio;

void main() => runApp(new Syth());

class Syth extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Aplikasi Report',
      home: new ReportUpload(),
    );
  }
}

class ReportUpload extends StatefulWidget {
  @override
  _ReportUploadState createState() => _ReportUploadState();
}

class _ReportUploadState extends State<ReportUpload> {
  File _image;

  Future openGallery() async {
    var imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image = imageFile;
    });
  }

  Future openCamera() async {
    var imageFile = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      _image = imageFile;
    });
  }

  Future upload(File imageFile) async {
    var stream =
        new http.ByteStream(DelegatingStream.typed(imageFile.openRead()));
    var length = await imageFile.length();
    var uri = Uri.parse('http://192.168.56.2:666/image-upload');
    var request = new http.MultipartRequest('POST', uri);
    var multipartFile = new http.MultipartFile('image', stream, length,
        filename: basename(imageFile.path));
    request.files.add(multipartFile);
    
    var response = await request.send();
    print(response.statusCode);

    if (response.statusCode == 200) {
      print('Berhasil Diupload');
    } else {
      print('Gagal Upload');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Laporan Berjangka'),
          backgroundColor: Colors.teal,
        ),
        body: Column(
          children: <Widget>[
            Center(
                child: _image == null
                    ? new Text('belum ada gambar di pilih')
                    : new Image.file(_image)),
            Center(
              child: Row(
                children: <Widget>[
                  RaisedButton(
                      child: Icon(Icons.image), onPressed: openGallery),
                  RaisedButton(
                      child: Icon(Icons.camera_alt), onPressed: openCamera),
                ],
              ),
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
            backgroundColor: Colors.teal,
            child: Icon(Icons.send),
            onPressed: () {
              upload(_image);
            }));
  }
}
